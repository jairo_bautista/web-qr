﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QRgenerator.aspx.cs" Inherits="WebApplication1.QRgenerator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID ="txtCode" runat="server"></asp:TextBox>
            <asp:Button ID ="btnGenerar1" runat="server" Text="Generar" OnClick="btnGenerar1_Click"/>
            <br /> <br />
            <asp:PlaceHolder ID="PHQRcode" runat="server"></asp:PlaceHolder>
        </div>
    </form>
</body>
</html>
