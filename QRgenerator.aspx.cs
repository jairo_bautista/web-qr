﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QRCoder;

namespace WebApplication1
{
    public partial class QRgenerator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGenerar1_Click(object sender, EventArgs e)
        {
            string Code = txtCode.Text;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeGenerator.QRCode qRCode = qrGenerator.CreateQrCode(Code, QRCodeGenerator.ECCLevel.Q);

            System.Web.UI.WebControls.Image ImgQRCode = new System.Web.UI.WebControls.Image();
            ImgQRCode.Height = 600;
            ImgQRCode.Width = 600;

            using (Bitmap bitmap = qRCode.GetGraphic(20))
            {
                 using(MemoryStream ms = new MemoryStream())
                 {
                    bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    byte[] byteImage = ms.ToArray();
                    ImgQRCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                 }

                PHQRcode.Controls.Add(ImgQRCode);

            }

        }
    }
}